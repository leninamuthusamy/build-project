package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPom;

//Rule 1: A class LoginPage is created for the first page.
//Rule 2: The class is inherited with common wrapper methods.
//Rule 3: A constructor is created and Page Factory concept is applied.
//Rule 4: 
public class Page1Login extends ProjectMethodsPom {
	public Page1Login() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using = "username") WebElement eleUserName;
	
	@FindBy(how=How.ID, using = "password") WebElement elePassword;
	
	@FindBy(how=How.CLASS_NAME, using = "decorativeSubmit") WebElement eleLogin;
	
	
	public Page1Login enterUserName(String uName) {
		//WebElement eleUserName = locateElement("id","username");
		type(eleUserName, uName);
		return this;
		
	}
	public Page1Login enterPassword(String pass) {
		type(elePassword, pass);
		return this;
	}
	public void clickLogin() {
		click(eleLogin);
		//return new Page2Home();
	}
	
	
}
