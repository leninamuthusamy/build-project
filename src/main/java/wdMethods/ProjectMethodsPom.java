package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import utils.ReadFile;

public class ProjectMethodsPom extends SeMethodsPom{


	/*protected String dataSheetName;*/
	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}
	@BeforeMethod
	public void login() {
	    beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
	
/*		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		Thread.sleep(2000);
		WebElement crm = locateElement("linkText", "CRM/SFA");
		click(crm);	*/	
	}
	
@DataProvider(name = "excel")
	public Object[][] getData() throws Exception{
		ReadFile dataFromSrc = new ReadFile();
		Object[][] sheet = dataFromSrc.dataRead(dataSheetName);
		return sheet;
	}
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	

}






